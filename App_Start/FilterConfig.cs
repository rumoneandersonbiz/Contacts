﻿using System.Web;
using System.Web.Mvc;

namespace Project1_Database_and_EF_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
