﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Project1_Database_and_EF_.Models
{
    public class addressBook
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name="Contact Number")]
        public string ContactNumber { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

    }


    public class addressBookDbContext : DbContext
    {
        public DbSet<addressBook> AddressBook { get; set; }
    }
}