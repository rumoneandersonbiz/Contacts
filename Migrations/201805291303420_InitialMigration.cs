namespace Project1_Database_and_EF_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.addressBooks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        ContactNumber = c.String(nullable: false, maxLength: 255),
                        Address = c.String(maxLength: 255),
                        Email = c.String(maxLength: 255),
                        Notes = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.addressBooks");
        }
    }
}
