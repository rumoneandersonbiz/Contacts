﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project1_Database_and_EF_.Models;

namespace Project1_Database_and_EF_.Controllers
{
    public class addressBooksController : Controller
    {
        private addressBookDbContext db = new addressBookDbContext();

        // GET: addressBooks
        public ActionResult Index()
        {
            return View(db.AddressBook.ToList());
        }

        // GET: addressBooks/Details/5
        public ActionResult Details(int id)
        {
            addressBook addressBook = db.AddressBook.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }
            return View(addressBook);
        }


        public ActionResult Create()
        {
            return View();
        }

        // POST: addressBooks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ContactNumber,Address,Email,Notes")] addressBook addressBook)
        {
            if (ModelState.IsValid)
            {
                db.AddressBook.Add(addressBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(addressBook);
        }

        // GET: addressBooks/Edit/5
        public ActionResult Edit(int id)
        {
            addressBook addressBook = db.AddressBook.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }
            return View(addressBook);
        }

        // POST: addressBooks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ContactNumber,Address,Email,Notes")] addressBook addressBook)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addressBook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(addressBook);
        }

        // GET: addressBooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            addressBook addressBook = db.AddressBook.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }
            return View(addressBook);
        }

        // POST: addressBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            addressBook addressBook = db.AddressBook.Find(id);
            db.AddressBook.Remove(addressBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
