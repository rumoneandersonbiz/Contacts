﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Project1_Database_and_EF_.Models;

namespace Project1_Database_and_EF_.Controllers.api
{
    public class addressBooksController : ApiController
    {
        private addressBookDbContext db = new addressBookDbContext();

        // GET: api/addressBooks
        public IQueryable<addressBook> GetAddressBook()
        {
            return db.AddressBook;
        }

        // GET: api/addressBooks/5
        [ResponseType(typeof(addressBook))]
        public IHttpActionResult GetaddressBook(int id)
        {
            addressBook addressBook = db.AddressBook.Find(id);
            if (addressBook == null)
            {
                return NotFound();
            }

            return Ok(addressBook);
        }

        // PUT: api/addressBooks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutaddressBook(int id, addressBook addressBook)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != addressBook.Id)
            {
                return BadRequest();
            }

            db.Entry(addressBook).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!addressBookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/addressBooks
        [ResponseType(typeof(addressBook))]
        public IHttpActionResult PostaddressBook(addressBook addressBook)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AddressBook.Add(addressBook);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = addressBook.Id }, addressBook);
        }

        // DELETE: api/addressBooks/5
        [ResponseType(typeof(addressBook))]
        public IHttpActionResult DeleteaddressBook(int id)
        {
            addressBook addressBook = db.AddressBook.Find(id);
            if (addressBook == null)
            {
                return NotFound();
            }

            db.AddressBook.Remove(addressBook);
            db.SaveChanges();

            return Ok(addressBook);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool addressBookExists(int id)
        {
            return db.AddressBook.Count(e => e.Id == id) > 0;
        }
    }
}